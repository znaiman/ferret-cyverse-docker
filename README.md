To change Jupyter Notebook password, update the file in jupyter/jupyter+notebook_config.py and build with docker.

in Jupyter:

%load_ext ferretmagic

%%ferret
use levitus_climatology
shade temp[k=1]
frame/file="sst.pdf" /yinches=11