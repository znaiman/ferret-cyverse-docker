#!/bin/bash

source /opt/miniconda3/bin/activate py27
source /usr/local/ferret/ferret_paths
pip install --upgrade ferretmagic
cd /root/IPyNCL/
python setup.py install
cd /persist
jupyter notebook --allow-root --ip=0.0.0.0 --port=8080 --no-browser
