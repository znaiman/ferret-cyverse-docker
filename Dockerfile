FROM centos
RUN yum install -y epel-release
RUN yum groupinstall -y 'Development Tools'
RUN yum install -y netcdf python-devel libevent-devel xeyes vim python-pip python-wheel xorg-x11-fonts-Type1
RUN yum install -y python-matplotlib ImageMagick tcsh libicu52 wget

RUN wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh -O miniconda.sh
RUN chmod +x miniconda.sh \
    && bash ./miniconda.sh -b -p /opt/miniconda3

RUN /opt/miniconda3/bin/conda create -n py27 python=2.7
RUN source /opt/miniconda3/bin/activate py27
RUN /opt/miniconda3/bin/conda install -n py27 pip

RUN pip install --upgrade pip
RUN pip install --upgrade auxlib ruamel.yaml urllib3 requests pycosat
RUN pip install --upgrade numpy



COPY ferret/ /usr/local/ferret/
RUN echo "source /usr/local/ferret/ferret_paths" >> /root/.bashrc
RUN echo "cancel mode journal;spawn cd /ferret/ferret_jnl/;set mode journal:/ferret/history/ferret.jnl" >> /root/.ferret

ENV NCARG_ROOT /usr/
RUN yum install -y  libicu
COPY channel.py /usr/lib/python2.7/site-packages/conda/models/channel.py


RUN /opt/miniconda3/bin/conda install -y -c conda-forge -n py27 hdf5 netcdf4 ncl nco cdo pyferret
RUN /opt/miniconda3/bin/conda install -y -c conda-forge -n py27 icu=58.1
RUN /opt/miniconda3/bin/conda install -y -c conda-forge -n py27 jupyter matplotlib
RUN /opt/miniconda3/bin/conda install -y -c conda-forge -n py27 nco=4.5

RUN pip install --upgrade ferretmagic

#RUN ln -s /ferret/jupyter/.jupyter /root/.jupyter
RUN mkdir -p /root/.jupyter
RUN chmod 755 /root/.jupyter
COPY jupyter/jupyter_notebook_config.py /root/.jupyter/
RUN ln -s /ferret/jupyter/ /root/jupyter
RUN echo "export PATH=$PATH:/opt/miniconda3/bin/" >> /root/.bashrc

RUN echo "export PATH=$PATH:/opt/miniconda3/bin/" >> /root/.bashrc
RUN echo "source /usr/local/ferret/ferret_paths" >> /root/.bashrc

COPY startup.sh /usr/bin/
RUN echo "chmod +x /usr/bin/startup.sh"


COPY IPyNCL/ /root/IPyNCL/
RUN cd /root/IPyNCL/

RUN echo "source /usr/bin/startup.sh" >> /root/.bashrc
